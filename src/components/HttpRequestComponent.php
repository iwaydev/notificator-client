<?php

namespace notificator\components;

/**
 * Класс компонент отправки Http запросов в очередь нотификатора
 *
 * Class SmsComponent
 * @package notificator\components
 */
class HttpRequestComponent extends BaseComponent
{
    const HTTP_REQUEST_QUEUE_NAME = 'notificator.http_request_by_send';

    /**
     * @var array Параметры продюсера
     */
    public $producerParameters = [
        'exchange_options' => [
            'name' => 'notificator.http_request_by_send',
            'type' => 'direct',
        ],
        'queue_options' => [
            'name' => 'notificator.http_request_by_send',
            'routing_keys' => ['notificator.http_request_by_send'],
            'durable' => true,
            'auto_delete' => false,
            'arguments' => [
                'x-message-ttl' => ['I', 1000 * 60 * 30] //Вермя жизни запросов в очереди 30мин
            ]
        ],
    ];

    /**
     * Базовая инициализация
     */
    public function init()
    {
        parent::init();
        $this->initProducer(self::HTTP_REQUEST_QUEUE_NAME, $this->producerParameters);
    }

    /**
     * Отправить данные для Http запроса в очередь нотификатора
     *
     * @param string $url Url
     * @param array $data Данные тела запроса
     * @param string|null $method Мотод передачи
     * @param null $headers Заголовки запроса
     */
    public function send(string $url, $data = [], string $method = null, $headers = null)
    {
        $data = [
            'url' => $url,
            'data' => $data,
            'method' => $method,
            'headers' => $headers,
        ];
        $this->sendMessageToQueue($data);
    }

    /**
     * Получить название очереди
     *
     * @return string
     */
    protected function getQueueName() :string
    {
        return self::HTTP_REQUEST_QUEUE_NAME;
    }
}