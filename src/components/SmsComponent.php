<?php

namespace notificator\components;

use notificator\daemon\producers\SmsBySendProducer;
use Yii;

/**
 * Класс компонент отправки sms в очередь нотификатора
 *
 * Class SmsComponent
 * @package notificator\components
 */
class SmsComponent extends BaseComponent
{
    const SMS_QUEUE_NAME = 'notificator.sms_by_send';

    /**
     * @var array Параметры продюсера
     */
    public $producerParameters = [
        'exchange_options' => [
            'name' => 'notificator.sms_by_send',
            'type' => 'direct',
        ],
        'queue_options' => [
            'name' => 'notificator.sms_by_send',
            'routing_keys' => ['notificator.sms_by_send'],
            'durable' => true,
            'auto_delete' => false,
            'arguments' => [
                'x-message-ttl' => ['I', 1000 * 60 * 30] //Вермя жизни запросов в очереди 30мин
            ]
        ],
    ];

    /**
     * Базовая инициализация
     */
    public function init()
    {
        parent::init();
        $this->initProducer(self::SMS_QUEUE_NAME, $this->producerParameters);
    }

    /**
     * Отправить данные для sms в очередь нотификатора
     *
     * @param string $phone Номер телефона
     * @param string $text Текст сообшения
     * @param string $provider Провайдер
     * @param string|null $uuid Уникальный индификатор
     */
    public function send(string $phone, string $text, string $provider, string $uuid = null)
    {
        $data = [
            'uuid' => empty($uuid) ? Yii::$app->security->generateRandomString() : $uuid,
            'phone' => $phone,
            'text' => $text,
            'provider' => $provider,
        ];
        $this->sendMessageToQueue($data);
    }

    /**
     * Получить название очереди
     *
     * @return string
     */
    protected function getQueueName() :string
    {
        return self::SMS_QUEUE_NAME;
    }
}