<?php

namespace notificator\components;

use mikemadisonweb\rabbitmq\components\AbstractConnectionFactory;
use mikemadisonweb\rabbitmq\components\BaseRabbitMQ;
use mikemadisonweb\rabbitmq\components\Producer;
use mikemadisonweb\rabbitmq\Configuration;
use yii\base\Component;
use Yii;
use yii\base\InvalidConfigException;
use yii\web\HttpException;

/**
 * Базовый класс для компонентов нотификатора
 *
 * Class NotificatorComponent
 * @package notificator\components
 */
abstract class BaseComponent extends Component
{
    const NOTIFICATOR_CONNECTION_KEY = 'notificator';

    /**
     * @var array Параметры логера rabbitmq
     */
    public $logger = [
        'enable' => true,
        'category' => 'rabbitmq',
        'print_console' => true,
        'system_memory' => true,
    ];
    /**
     * @var array Параметры подключения к rabbitmq notificator
     */
    public $connection = [];

    /**
     * Базовая инициализация
     */
    public function init()
    {
        parent::init();
        $this->initNotificatorConnection();
    }

    /**
     * Инициализация подключения к rabbitmq notificator
     *
     * @throws InvalidConfigException
     */
    protected function initNotificatorConnection()
    {
        if(empty($this->connection)) {
            throw new InvalidConfigException(get_class($this) . '::$connection must be set.');
        }
        $connection = $this->connection;
        $serviceAlias = sprintf(BaseRabbitMQ::CONNECTION_SERVICE_NAME, self::NOTIFICATOR_CONNECTION_KEY);
        if(Yii::$container->has($serviceAlias)) {
            return;
        }
        \Yii::$container->set($serviceAlias, function () use ($connection) {
            $factory = new AbstractConnectionFactory(Configuration::CONNECTION_CLASS, $connection);
            return $factory->createConnection();
        });
    }

    /**
     * Инициализация продюсера для очереди notificator
     *
     * @param string $key Ключ очереди
     * @param array $parameters Параметры продюсера
     */
    protected function initProducer(string $key, array $parameters)
    {
        $serviceAlias = sprintf(BaseRabbitMQ::PRODUCER_SERVICE_NAME, $key);
        if(Yii::$container->has($serviceAlias)) {
            return;
        }
        if(!isset($parameters['connection'])) {
            $parameters['connection'] = self::NOTIFICATOR_CONNECTION_KEY;
        }
        Yii::$container->set($serviceAlias, function () use ($key, $parameters) {
            $connection = \Yii::$container->get(sprintf('rabbit_mq.connection.%s', $parameters['connection']));
            $producer = new Producer($connection);

            //this producer doesn't define an exchange -> using AMQP Default
            if (!isset($parameters['exchange_options'])) {
                $parameters['exchange_options'] = [];
            }
            \Yii::$container->invoke([$producer, 'setExchangeOptions'], [$parameters['exchange_options']]);

            //this producer doesn't define a queue -> using AMQP Default
            if (!isset($parameters['queue_options'])) {
                $parameters['queue_options'] = [];
            }
            \Yii::$container->invoke([$producer, 'setQueueOptions'], [$parameters['queue_options']]);

            if (isset($parameters['auto_setup_fabric']) && !$parameters['auto_setup_fabric']) {
                \Yii::$container->invoke([$producer, 'disableAutoSetupFabric']);
            }

            \Yii::$container->invoke([$producer, 'setLogger'], [$this->logger]);

            return $producer;
        });
    }

    /**
     * Отправка сообщения в очередь.
     *
     * @param mixed $message
     * @return $this
     */
    public function sendMessageToQueue($message)
    {
        $producer = Yii::$container->get(sprintf(BaseRabbitMQ::PRODUCER_SERVICE_NAME, $this->getQueueName()));
        $producer->publish(json_encode($message), $this->queueName);

        return $this;
    }

    /**
     * Получить название очереди
     *
     * @return string
     */
    abstract protected function getQueueName() :string;
}