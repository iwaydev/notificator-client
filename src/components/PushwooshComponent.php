<?php

namespace notificator\components;

/**
 * Класс компонент отправки Pushwoosh запросов в очередь нотификатора
 *
 * Class SmsComponent
 * @package notificator\components
 */
class PushwooshComponent extends BaseComponent
{
    const PUSHWOOSH_QUEUE_NAME = 'notificator.pushwoosh';

    /**
     * @var array Параметры продюсера
     */
    public $producerParameters = [
        'exchange_options' => [
            'name' => 'notificator.pushwoosh',
            'type' => 'direct',
        ],
        'queue_options' => [
            'name' => 'notificator.pushwoosh',
            'routing_keys' => ['notificator.pushwoosh'],
            'durable' => true,
            'auto_delete' => false,
            'arguments' => [
                'x-message-ttl' => ['I', 1000 * 60 * 30] //Вермя жизни запросов в очереди 30мин
            ]
        ],
    ];

    /**
     * Базовая инициализация
     */
    public function init()
    {
        parent::init();
        $this->initProducer(self::PUSHWOOSH_QUEUE_NAME, $this->producerParameters);
    }

    /**
     * Отправить данные для Pushwoosh в очередь нотификатора
     *
     * @param string $content Контент
     * @param array $devices Массив Id устройств
     * @param array $data Дополнительные данные
     * @param array $additionalParams Дополнительные параметры
     */
    public function send(string $content, array $devices, $data = [], $additionalParams = [])
    {
        $data = [
            'content' => $content,
            'devices' => $devices,
            'data' => $data,
            'additionalParams' => $additionalParams,
        ];
        $this->sendMessageToQueue($data);
    }

    /**
     * Получить название очереди
     *
     * @return string
     */
    protected function getQueueName() :string
    {
        return self::PUSHWOOSH_QUEUE_NAME;
    }
}